// CRUD OPERATIONS

/*
	- MongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into out methods
*/


// CREATE: Inserting documents

// INSERT ONE
/*
	Syntax:
		db.collectionName.insertOne({})

	Inserting/Assignning values in JS Objects:
		object.object.method({object})
*/

db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	courses: ['CSS', 'JavaScript', 'Python'],
	department: "none"
});


// INSERT MANY
/*
	Syntax:
	db.collectionName.insertMany([
		{objectA},
		{objectB}
	])
*/

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "5878787",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "5552525",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
]);



// READ: FINDING DOCUMENTS

// FIND ALL
/*
	Syntax:
		db.collectionName.find();
*/
db.users.find();


// Finding users with single arguments
/*
	Syntax:
	db.collectionName.find({ field: value })
*/
db.users.find({ firstName: "Stephen" });


// no match
db.users.find({ firstName: "Stephen", age: 20 });

// 1 match found
db.users.find({ firstName: "Stephen", age: 76 });




// UPDATE
// Repeat Jane to be updated
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	courses: ['CSS', 'JavaScript', 'Python'],
	department: "none"
});


// UPDATE ONE
/*
	Syntax:
		db.collectionName.updateOne({criteria}, {$set: {field:value}} )
*/
db.users.updateOne(
	{firstName: "Jane"}, 
	{
		$set: {
			firstName: "Jane",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "5875698",
				email: "janedoe@gmail.com"
			},
			courses: ['AWS', 'Google Cloud', 'Azure'],
			department: "none",
			status: "active"
		}
	}
);
db.users.find({firstName: "Jane"});


// UPDATE MANY
db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
		}
	}
)
db.users.find().pretty();


// REPLACE ONE
/*
	- Can be used if replacing the whole document is necessary
	- Syntax:
		db.collectionName.replaceOne({criteria}, {field: value})
*/
db.users.replaceOne(
	{ lastName: "Gates" },
    {
		firstName: "Bill",
		lastName: "Clinton"
	}
)
db.users.find({firstName: "Bill"});



// DELETE: DELETING DOCUMENTS
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});
db.users.find();

// DELETING A DOCUMENT
/*
	Syntax:
		db.collectionName.deleteOne({criteria})
*/
db.users.deleteOne({firstName: "Test"});
db.users.find({firstName: "Test"});


// DELETE MANY
db.users.deleteMany({
	courses: []
});
db.users.find();

// DELETE ALL
// db.collectionName.delete() or db.collectionName.deleteMany()


// ADVANCED QUERIES

// Query an embedded document
db.users.find({
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	}
});

// Find the document withe email "janedoe@gmail.com"
// Querying on nested fields
db.users.find({
	"contact.email": "janedoe@gmail.com"
});

// Querying an array with exact elements
db.users.find({"courses": ["React", "Laravel", "SASS"]});

// Querying an element without regard to order
db.users.find({"courses": {$all:["React", "SASS", "Laravel"]}});

// Make an array to query
db.users.insert({
	namearr: [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}
	]
});

// find
db.users.find({
	namearr: {
		namea: "juan"
	}
});
